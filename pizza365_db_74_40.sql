-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 09, 2023 at 02:15 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza365_db_74`
--

-- --------------------------------------------------------

--
-- Table structure for table `drink`
--

CREATE TABLE `drink` (
  `id` int(10) NOT NULL,
  `drink_code` varchar(40) NOT NULL,
  `drink_name` varchar(40) NOT NULL,
  `total_money` int(10) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drink`
--

INSERT INTO `drink` (`id`, `drink_code`, `drink_name`, `total_money`, `create_date`, `update_date`) VALUES
(1, 'COCA', 'cô ca co la', 10000, 1683589233, 1683589233),
(2, 'LAVI', 'lavi', 10000, 1683589301, 1683589301),
(3, 'BH', 'Bò húc', 10000, 1683589301, 1683589301),
(4, 'SUA', 'Tra Sua', 10000, 1683589301, 1683589301);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) NOT NULL,
  `order_code` varchar(40) NOT NULL,
  `pizza_size` varchar(40) NOT NULL,
  `pizza_type` varchar(40) NOT NULL,
  `voucher_id` int(10) NOT NULL,
  `total_money` int(10) NOT NULL,
  `discount` int(10) DEFAULT NULL,
  `fullname` varchar(40) NOT NULL,
  `email` varchar(40) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `drink_id` int(10) NOT NULL,
  `status_id` int(10) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `pizza_size`, `pizza_type`, `voucher_id`, `total_money`, `discount`, `fullname`, `email`, `address`, `drink_id`, `status_id`, `create_date`, `update_date`) VALUES
(1, '52352', 'S', 'Hun Khói', 1, 10000, 20, 'nguyen huu thuong', 'nguyenhuuthuong04041999@gmail.com', 'VN', 1, 4, 1683590886, 1683590886),
(2, '3134', 'M', 'Hải sản', 1, 10000, 40, 'Ha Sỹ Huy', 'A@gamil.com', 'Trung QUốc', 1, 2, 1683591036, 1683591036),
(3, '34135', 'L', 'Hải sản', 1, 20000, 10, 'Lê Mỹ Trung', 'Trung@gamil.com', 'Bình Định ', 1, 1, 1683591127, 1683591127),
(4, '5245243', 'S', 'Ba Con', 2, 30000, 20, 'Lê Thị Thảo', 'thao@gamil.com', 'Thọ Tiến', 2, 2, 1683591220, 1683591220),
(5, '65434523', 'M', 'Hải sản', 3, 20000, 10, 'Nguyễn Hữu Tài', 'tai@gmail.com', 'Thọ Tiến', 4, 2, 1683591292, 1683591292);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) NOT NULL,
  `role_code` varchar(40) NOT NULL COMMENT 'ma role\r\nma role',
  `role_name` varchar(40) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_code`, `role_name`, `create_date`, `update_date`) VALUES
(1, 'ad', 'admin', 1683531458, 1683531458),
(2, 'ga', 'ga', 1683531680, 1683531680),
(3, 'DC', 'Động cơ', 1683531680, 1683531680),
(4, 'DT', 'Điện Thoai', 1683531680, 1683531680),
(5, 'MT', 'Máy tính', 1683531680, 1683531680);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(10) NOT NULL,
  `status_code` varchar(40) NOT NULL,
  `status_name` varchar(40) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status_code`, `status_name`, `create_date`, `update_date`) VALUES
(1, 'CANCEL', 'cancel', 1683590408, 1683590408),
(2, 'CONFIRM', 'confirm', 1683590600, 1683590600),
(4, 'PRO', 'Processing', 1683590600, 1683590600);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `user_name` varchar(40) NOT NULL,
  `first_name` varchar(40) NOT NULL,
  `last_name` varchar(40) NOT NULL,
  `email` varchar(40) DEFAULT NULL,
  `role_id` int(10) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `first_name`, `last_name`, `email`, `role_id`, `create_date`, `update_date`) VALUES
(1, 'thuong', 'nguyen', 'huu', 'nguyenhuuthuong04041999@gmail.com', 1, 1683534288, 1683534288),
(2, 'linh', 'nguyen', 'huu', 'nguyenhuuA04041999@gmail.com', 1, 1683534382, 1683534382),
(3, 'nguyen', 'nguyen', 'huu', 'nguyenhuuB04041999@gmail.com', 2, 1683534382, 1683534382),
(4, 'tai', 'nguyen', 'huu', 'nguyenhuuC@gmail.com', 3, 1683534382, 1683534382),
(5, 'tuan', 'nguyen', 'huu', 'nguyenhuuD@gmail.com', 4, 1683534382, 1683534382);

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(10) NOT NULL,
  `voucher_code` varchar(40) NOT NULL,
  `discount` int(10) NOT NULL,
  `is_used_yn` varchar(40) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `voucher_code`, `discount`, `is_used_yn`, `create_date`, `update_date`) VALUES
(1, '312341', 20, 'yes', 1683588815, 1683588815),
(2, '312541', 20, 'yes', 1683589155, 1683589155),
(3, '312581', 60, 'yes', 1683589155, 1683589155),
(4, '312547', 90, 'yes', 1683589155, 1683589155);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `drink`
--
ALTER TABLE `drink`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_drink_code_drink` (`drink_code`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_order_code_user` (`order_code`),
  ADD KEY `voucher_id` (`voucher_id`),
  ADD KEY `drink_id` (`drink_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_index_code` (`role_code`),
  ADD KEY `create_date` (`create_date`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_status_code_status` (`status_code`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_name_code` (`user_name`),
  ADD UNIQUE KEY `index_email_user` (`email`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_voucher_code_voucher` (`voucher_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `drink`
--
ALTER TABLE `drink`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`voucher_id`) REFERENCES `vouchers` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`drink_id`) REFERENCES `drink` (`id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
